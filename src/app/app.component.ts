import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

interface City {
  name: string;
  code: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  movieNames: Array<string> = [];
  selectedMovie: string | null = null;
  userRating: number = 5;
  movies: any = {};
  recommendations: any = [];
  constructor(private http: HttpClient, private spinner: NgxSpinnerService) {}
  ngOnInit() {
    this.spinner.show();
    this.http.get('assets/similarity_scores.json').subscribe((data) => {
      this.spinner.hide();
      this.movies = data;

      this.movieNames = Object.keys(data);
      this.movieNames = this.movieNames.sort();
    });
  }
  onGo() {
    this.recommendations = [];
    if (this.selectedMovie) {
      // Create items array
      let dict = JSON.parse(JSON.stringify(this.movies[this.selectedMovie]));
      for (let key in dict) {
        dict[key] = dict[key] * (this.userRating - 2.5);
      }
      let items = Object.keys(dict).map(function (key) {
        return [key, dict[key]];
      });

      // Sort the array based on the second element
      items.sort(function (first, second) {
        return second[1] - first[1];
      });

      this.recommendations = items.slice(1, 10);
      console.log(this.recommendations);
    } else {
    }
  }
}
